﻿const starsElements = [
    document.getElementById("star1"),
    document.getElementById("star2"),
    document.getElementById("star3"),
    document.getElementById("star4"),
    document.getElementById("star5"),
    document.getElementById("star6"),
    document.getElementById("star7"),
    document.getElementById("star8"),
    document.getElementById("star9"),
    document.getElementById("star10"),
    document.getElementById("star11"),
    document.getElementById("star12")
];



const angleStep=60;

const timerContener=document.getElementById("timer-contener");

var xCenter;
var yCenter;
var xp;
var yp;
var radius=72;
var topTimerContener;
var leftTimerContner;
var clockcomputedstyles;
const starsQuantity = starsElements.length;
const biggerStarPath = "star_bigger.png";

var actualStar = 0;
var actualPath = "";

const elementTimer = document.getElementById("timer");

const thisDate = new Date().getTime();
const oneHourDistance = thisDate + (60 * 60 * 1000);

window.onload=load;

function load() {
    ClaculateclockCenter();
    //calculateCoordinates(0);
    SetStylesForStars();
    const myTimer = setInterval(function () {
        const thisTime = new Date().getTime();
        const diff = oneHourDistance - thisTime;
         const minutesNumber = Math.floor(diff / (1000 * 60));
        if (minutesNumber < 10) {
            minutes = '0' + minutesNumber.toString();
        }
        else minutes = minutesNumber.toString();
        const secondsNumber = Math.floor((diff / 1000) - minutes * 60);
        if (secondsNumber < 10) {
            seconds = '0' + secondsNumber.toString();
        }
        else seconds = secondsNumber.toString();
         const textTime = minutes + ':' + seconds;
        elementTimer.innerHTML = "";
        elementTimer.innerHTML = textTime;
         changeImagePath();
        if (secondsNumber + minutesNumber == 0) {
            clearInterval(myTimer);
        }
    }, 1000)
    
    
}

function convertToRadian(angle)
{
    return (angle/360)* Math.PI*2;
}


function calculateCoordinates(angle) {
    let angleRadian=convertToRadian(angle);
    xp=radius*Math.cos(angleRadian)+radius;
    yp=radius*Math.sin(angleRadian)+radius;
}


function ClaculateclockCenter() {
    clockcomputedstyles=window.getComputedStyle(timerContener);
    let width=parseInt(clockcomputedstyles.getPropertyValue("width"));
    leftTimerContener=parseInt(clockcomputedstyles.getPropertyValue("left"));
    let height=parseInt(clockcomputedstyles.getPropertyValue("height"));
    topTimerContener=parseInt(clockcomputedstyles.getPropertyValue("top"));
    xCenter=leftTimerContener+width/2;
    yCenter=topTimerContener+height/2;
    // radius=width/2 -1;
}

function SetStylesForStars() {
    let Angle=0;
    for (i=0;i<starsQuantity;i++) {
        calculateCoordinates(Angle);
        starsElements[i].style.position="absolute";
        starsElements[i].style.backgroundColor="transparent";
        // xp=160;
        let ypos=Math.round(yp)+"px";
        let xpos=Math.round(xp)+"px";
        starsElements[i].style.top=/*(topTimerContener).*/ypos;/*"5px";*/
        starsElements[i].style.left=/*leftTimerContener.*/xpos;/* "60px";*/
        Angle=Angle+30;
    }
}




function changeImagePath() {
    if (actualStar == 0) {
        actualPath = starsElements[actualStar].src;
        starsElements[actualStar].src = biggerStarPath;
        actualStar++;
    }
    else if (actualStar < starsQuantity) {
        starsElements[actualStar - 1].src = actualPath;
        actualPath = starsElements[actualStar].src;
        starsElements[actualStar].src = biggerStarPath;
        actualStar++;
    }
    else {
        starsElements[actualStar - 1].src = actualPath;
        actualStar = 0;
        actualPath = starsElements[actualStar].src;
        starsElements[actualStar].src = biggerStarPath;
        actualStar++;
}

}



// const myTimer = setInterval(function () {
//     const thisTime = new Date().getTime();
//     const diff = oneHourDistance - thisTime;
//      const minutesNumber = Math.floor(diff / (1000 * 60));
//     if (minutesNumber < 10) {
//         minutes = '0' + minutesNumber.toString();
//     }
//     else minutes = minutesNumber.toString();
//     const secondsNumber = Math.floor((diff / 1000) - minutes * 60);
//     if (secondsNumber < 10) {
//         seconds = '0' + secondsNumber.toString();
//     }
//     else seconds = secondsNumber.toString();
//      const textTime = minutes + ':' + seconds;
//     elementTimer.innerHTML = "";
//     elementTimer.innerHTML = textTime;
//      changeImagePath();
//     if (secondsNumber + minutesNumber == 0) {
//         clearInterval(myTimer);
//     }
// }, 1000)

