const projects = document.getElementById("projects");
projects.addEventListener("click", goToProjects);

function goToProjects() {
      window.location.href = "#redRectangle__skills-header";
}

const aboutMe = document.getElementById("aboutMe");
aboutMe.addEventListener("click", goToAboutMe);
function goToAboutMe() {
      window.location.href = "aboutMe";
}

const contactButton = document.getElementById("contact");
contactButton.addEventListener("click", goToContact);
function goToContact() {
      window.location.href = "#contact-wrapper";
}

const myProjects = document.getElementById("main-presentation__myProjects");
myProjects.addEventListener("click", goToProjects);

const skillWindow = document.getElementsByClassName("skillWindow");


const redRectangle = document.getElementById("redRectangle");
redRectangle.addEventListener("click", hideSkillwindow)

function hideSkillwindow() {
      for (n = 0; n < skillWindow.length; n++) {
            if (skillWindow[n].style.visibility === "visible") {
                  skillWindow[n].style.visibility = "hidden";
            }
      }
      closingMessage.style.visibility = "hidden";
}

function ScrollWindow() {
      let ww = window.innerWidth;
      let cc=0;
      if (ww>800) {
            cc=0.9;
      }
      else
      if ((ww<800) && (ww>500)) {
            cc=1.2;
      }
      else {
            cc=1.5;
      }
      // if (ww > 800) {
            let Height = screen.height;
            var scrollOptions = {
                  left: 0,
                  top: Math.ceil(Height * cc),
                  behavior: 'smooth'
            }
            window.scroll(scrollOptions);
      // }
}


var prevScrollpos = window.pageYOffset;
window.onscroll = function () {
      var currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
            document.getElementById("main-nav").style.top = "0";
      } else {
            document.getElementById("main-nav").style.top = "-60px";
      }
      prevScrollpos = currentScrollPos;
}


const CandCplus = document.getElementById("skills-container__liCandCplus");
const languageCandCplusWindow = document.getElementById("skills-container__languageCandCplusWindow");

CandCplus.addEventListener("click", CplusSetVisible);





function CplusSetVisible() {
      hideSkillwindow();
      languageCandCplusWindow.style.visibility = "visible";
      closingMessage.style.visibility = "visible";
      ScrollWindow();
}

const liCsharp = document.getElementById("skills-container__liCsharp");//.getAnimations      //
const languageCsharpWindow = document.getElementById("skills-container__languageCsharpWindow");

liCsharp.addEventListener("click", CsharpSetVisible);

function CsharpSetVisible() {
      hideSkillwindow();
      languageCsharpWindow.style.visibility = "visible";
      closingMessage.style.visibility = "visible";
      ScrollWindow();
}

const languageJavaWindow = document.getElementById("skills-container__languageJavaWindow");
const liJava = document.getElementById("skills-container__liJava");
liJava.addEventListener("click", JavaSetVisible);

function JavaSetVisible() {
      hideSkillwindow();
      languageJavaWindow.style.visibility = "visible";
      closingMessage.style.visibility = "visible";
      ScrollWindow();
}

const liJS = document.getElementById("skills-container__liJS");
liJS.addEventListener("click", JSSetVisible)
const languageJSWindow = document.getElementById("skills-container__languageJSWindow");

function JSSetVisible() {
      hideSkillwindow();
      languageJSWindow.style.visibility = "visible";
      closingMessage.style.visibility = "visible";
      ScrollWindow();
}


liCSS = document.getElementById("skills-container__liCSS");
liCSS.addEventListener("click", HTML_CSSSetVidible);

liHTML = document.getElementById("skills-container__liHTML");
liHTML.addEventListener("click", HTML_CSSSetVidible);

const HTML_CSSWindow = document.getElementById("skills-container__HTML_CSSWindow");

function HTML_CSSSetVidible() {
      hideSkillwindow();
      HTML_CSSWindow.style.visibility = "visible";
      closingMessage.style.visibility = "visible";
      ScrollWindow();
}

const closingMessage = document.getElementById("closingMessage");



